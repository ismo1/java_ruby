package com.jruby.app;
import org.jruby.embed.ScriptingContainer;
import org.jruby.runtime.builtin.IRubyObject;

/**
 *
 * Make a package and compile all => mvn package
 * Make a package with all dependencies => mvn clean dependency:copy-dependencies package
 * to execute app => java -cp target/my-app-1.0-SNAPSHOT.jar:target/dependency/jruby-complete-9.0.5.0.jar com.jruby.app.App
 *
 * Hello world!
 *
 */
public class App
{

  public void sayRbHello1(){
    ScriptingContainer container = new ScriptingContainer();
    container.runScriptlet("puts 'Hello from Ruby!'");
  }

  public void sayRbHello2(){
    ScriptingContainer container = new ScriptingContainer();
    container.put("message", "Hello from Java with help from Ruby!");
    IRubyObject result = (IRubyObject) container.runScriptlet("message.upcase");
    System.out.println(result);
  }


    public static void main( String[] args )
    {
      System.out.println( "Hello World!" );

      ScriptingContainer container = new ScriptingContainer();
      container.put("message", "Hello from Java with help from Ruby!");
      container.runScriptlet("puts message.upcase");

      container.put("message2", "Convert markdown to html via maruku gem");

      container.runScriptlet("puts message2");

      container.runScriptlet("require 'maruku'");
      container.runScriptlet("File.write('/Users/ismo1/jruby/docs/doc1.html', Maruku.new(File.read('/Users/ismo1/jruby/docs/doc.markdown')).to_html_document)");

    }
}
