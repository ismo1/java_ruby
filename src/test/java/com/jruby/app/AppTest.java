package com.jruby.app;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.jruby.embed.ScriptingContainer;
import org.jruby.runtime.builtin.IRubyObject;

/**
 * Unit test for simple App.
 */
public class AppTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    /**
     * Test ruby sass response :-)
     */
    public void testJavaSass() throws Exception {}
    {

      ScriptingContainer container = new ScriptingContainer();
      container.runScriptlet("require 'rubygems'; require 'sass';");

      String sass = ".test\n\tcolor: red";
      container.put("str", sass);

      String css = (String)container.runScriptlet("Sass::Engine.new(str).render");

      assertEquals(".test {\n  color: red; }\n", css);
      assertTrue( true );
    }
}
